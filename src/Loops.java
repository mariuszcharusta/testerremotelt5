public class Loops {
    public static void main(String[] args) {
        System.out.println("for loop");
        for (int i =5; i<5;i++){
            System.out.println(i);
        }

        System.out.println("do while loop");
        int x=5;
        do{
            System.out.println(x);
        }while (x<5);

        int i =0;

        System.out.println("\nWhile loop");
        while (i<5){
            System.out.println(i);
            i++;
        }

        int j =0;
        while (true){
            System.out.println(j);
            if(j==100)
                break;
            j++;
        }
    }
}
