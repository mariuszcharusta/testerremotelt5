import java.util.Scanner;

public class HelloWorld {
    public static void main(String[] args){
        System.out.println("Hello World!!");
        System.out.println("The same result");

        short myIntVariable = 10000;
        System.out.println(myIntVariable);

        boolean myBoolean = false;

        int myByte=128;

        //please check byte, short, long, boolean
        // what will happen if you assign value 128 to byte variable

        //double, float and char example
        double myDoule = 1.23;
        float myFloat = 1.23f;
        char myChar = 'a';
        char myChar2= '%';
        char myChar3= '3';
        String myText = "This is my text";
        System.out.println(myText);
        myText="This is another text";
        System.out.println(myText);
        System.out.println(myDoule);

        System.out.println(3==3);
        System.out.println(3!=3);
        System.out.println(5>3);
        System.out.println(3+1);
        // && - AND
        // || - OR
        System.out.println( 3==3 && 3!=7); // logical AND
        System.out.println(3==3 || 3!=3); // logical OR
        System.out.println(!true);//negation

        String text="sjhka";
        text.toUpperCase();

        //lenght, isEmpty, toLowerCase, toUpperCase, contains, indexOf

        String textToCompare="12345.";
        System.out.println(textToCompare.equals("12345.")); //equals compares two strings

        String textToCut="this is a text.";
        System.out.println(textToCut.split(" "));

        System.out.println(textToCompare.replace('.','!'));

        int a = 1;
        {
            a = 10; // we can use a variable from an external block
            int b = 20; // we can use the variable from the current block
            if (a == 10) {
                a = 100; b = 200;
                int c = 300;
            }
            System.out.println(a); // will print 100
            System.out.println(b); // will print 300
//            System.out.println(c); // compilation error; you cannot use a variable from an internal block
        }

        Scanner scanner = new Scanner(System.in);
        double myDouble = scanner.nextDouble(); //nextInt for int nextDouble for double etc
        System.out.println(myDouble);


    }
}